/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author montaguti
 */
public class MatrizInvalidaException extends Exception {
    
    private final Matriz m;
    
    public MatrizInvalidaException(Matriz mat) {
               super(String.format(
            "Matrizes de %dx%d não pode ser criada",
            mat.getMatriz().length, mat.getMatriz()[0].length));
            this.m = mat;
    }
    
    public int getNumLinhas() {
        return this.m.getMatriz().length;
    }
    
    public int getNumColunas() {
        return this.m.getMatriz()[0].length;
    }
    
}
